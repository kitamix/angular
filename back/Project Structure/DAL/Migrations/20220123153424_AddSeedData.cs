﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 0, new DateTime(2020, 10, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mertz - Rath" },
                    { 1, new DateTime(2021, 4, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Osinski - Von" },
                    { 2, new DateTime(2021, 11, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ernser, Dach and Paucek" },
                    { 3, new DateTime(2018, 6, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Heaney - Tillman" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 0, new DateTime(2000, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rita.Murphy@gmail.com", "Rita", "Murphy", new DateTime(2019, 10, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), 0 },
                    { 1, new DateTime(2000, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Courtney.McGlynn13@yahoo.com", "Courtney", "McGlynn", new DateTime(2017, 10, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 2, new DateTime(1966, 7, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Danny_Bernier56@hotmail.com", "Danny", "Bernier", new DateTime(2017, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 3, new DateTime(1966, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Julius.Beahan7@yahoo.com", "Julius", "Beahan", new DateTime(2021, 4, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 0, 0, new DateTime(2020, 12, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 2, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Qui ex architecto qui.", "next generation", 0 },
                    { 1, 1, new DateTime(2020, 9, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Unde ullam voluptatem eligendi architecto.", "Data", 1 },
                    { 2, 2, new DateTime(2021, 7, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 2, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Et sunt recusandae et quo dolores.", "Liaison pricing structure Estates", 2 },
                    { 3, 3, new DateTime(2020, 12, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 3, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Porro reprehenderit aliquid quo enim enim.", "Arkansas e-tailers PNG", 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 0, new DateTime(2018, 9, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Magni fugit ut consequatur sit voluptatem eum.", null, "Generic Steel Towels", 0, 0, 0 },
                    { 1, new DateTime(2018, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sit ut quo tempora.", null, "Producer", 0, 0, 1 },
                    { 2, new DateTime(2020, 4, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Et omnis cumque dolorem qui cupiditate aperiam.", new DateTime(2021, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lake Senior", 0, 0, 2 },
                    { 3, new DateTime(2020, 4, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Et iusto aut impedit.", null, "vertical", 0, 0, 3 },
                    { 4, new DateTime(2019, 11, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Expedita alias unde vitae esse quia voluptatem dolorum ullam aut.", null, "Baby Riel", 1, 1, 0 },
                    { 5, new DateTime(2019, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ut ut aliquam nihil dignissimos beatae rerum voluptatem et aut.", null, "Human Granite", 1, 1, 1 },
                    { 6, new DateTime(2018, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Placeat esse reprehenderit velit ratione quibusdam officiis dolor expedita.", new DateTime(2021, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "mobile", 1, 1, 2 },
                    { 7, new DateTime(2019, 1, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Laboriosam provident officiis quia quia unde et vitae.", null, "Specialist bus", 1, 1, 3 },
                    { 8, new DateTime(2019, 11, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Recusandae odit et sunt exercitationem maiores sapiente.", null, "multi-byte", 2, 2, 0 },
                    { 9, new DateTime(2018, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Voluptas rem eius perspiciatis ut sunt animi ratione temporibus magni.", null, "Key", 2, 2, 1 },
                    { 10, new DateTime(2019, 3, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Et ut inventore dolorem et ullam vero ratione.", new DateTime(2021, 5, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Berkshire invoice Small Rubber Salad", 2, 2, 2 },
                    { 11, new DateTime(2020, 10, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Est possimus ut doloremque placeat.", null, "Saint Martin hack Small", 2, 2, 3 },
                    { 12, new DateTime(2019, 6, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Beatae debitis animi quam temporibus accusamus modi.", null, "Mongolia Ergonomic Rubber Chicken", 3, 3, 0 },
                    { 13, new DateTime(2018, 11, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eos nobis eius aut consequuntur ut ea ut eum.", null, "AI Forward", 3, 3, 1 },
                    { 14, new DateTime(2018, 6, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Dignissimos nesciunt non omnis laborum.", new DateTime(2021, 7, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cambridgeshire Avon", 3, 3, 2 },
                    { 15, new DateTime(2020, 4, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Corrupti perferendis vel delectus placeat.", null, "Lari", 3, 3, 3 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
