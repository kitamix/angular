﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Models
{
    public class Project
    {
        public int Id { get; set; }

        public User Author { get; set; }

        public int AuthorId { get; set; }

        public Team Team { get; set; }

        public int TeamId { get; set; }

        public List<Task> Tasks { get; set; } = new List<Task>();

        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}