﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructure.DAL
{
    public class ProjectStructureDbContext : DbContext
    {
        public  ProjectStructureDbContext(DbContextOptions<ProjectStructureDbContext> options)
            : base(options)
        {}

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var projects = new List<Project>
            {
                new Project
                {
                    Id = 0,
                    AuthorId = 0,
                    TeamId = 0,
                    Name = "next generation",
                    Description = "Qui ex architecto qui.",
                    Deadline = new DateTime(2022, 2, 15),
                    CreatedAt = new DateTime(2020, 12, 16)
                },
                new Project
                {
                    Id = 1,
                    AuthorId = 1,
                    TeamId = 1,
                    Name = "Data",
                    Description = "Unde ullam voluptatem eligendi architecto.",
                    Deadline = new DateTime(2022, 5, 3),
                    CreatedAt = new DateTime(2020, 9, 19)
                },
                new Project()
                {
                    Id = 2,
                    AuthorId = 2,
                    TeamId = 2,
                    Name = "Liaison pricing structure Estates",
                    Description = "Et sunt recusandae et quo dolores.",
                    Deadline = new DateTime(2022, 2, 18),
                    CreatedAt = new DateTime(2021, 7, 5)
                },
                new Project()
                {
                    Id = 3,
                    AuthorId = 3,
                    TeamId = 3,
                    Name = "Arkansas e-tailers PNG",
                    Description = "Porro reprehenderit aliquid quo enim enim.",
                    Deadline = new DateTime(2022, 3, 9),
                    CreatedAt = new DateTime(2020, 12, 13)
                }
            };
            var users = new List<User>
            {
                new User()
                {
                    Id = 0,
                    TeamId = 0,
                    FirstName = "Rita",
                    LastName = "Murphy",
                    Email = "Rita.Murphy@gmail.com",
                    RegisteredAt = new DateTime(2019, 10, 7),
                    BirthDay = new DateTime(2000, 1, 20)
                },
                new User()
                {
                    Id = 1,
                    TeamId = 1,
                    FirstName = "Courtney",
                    LastName = "McGlynn",
                    Email = "Courtney.McGlynn13@yahoo.com",
                    RegisteredAt = new DateTime(2017, 10, 7),
                    BirthDay = new DateTime(2000, 1, 20)
                },
                new User()
                {
                    Id = 2,
                    TeamId = 2,
                    FirstName = "Danny",
                    LastName = "Bernier",
                    Email = "Danny_Bernier56@hotmail.com",
                    RegisteredAt = new DateTime(2017, 6, 19),
                    BirthDay = new DateTime(1966, 7, 14)
                },
                new User()
                {
                    Id = 3,
                    TeamId = 3,
                    FirstName = "Julius",
                    LastName = "Beahan",
                    Email = "Julius.Beahan7@yahoo.com",
                    RegisteredAt = new DateTime(2021, 4, 17),
                    BirthDay = new DateTime(1966, 10, 15)
                }
            };
            var teams = new List<Team>
            {
                new Team()
                {
                    Id = 0,
                    Name = "Mertz - Rath",
                    CreatedAt = new DateTime(2020, 10, 2)
                },
                new Team()
                {
                    Id = 1,
                    Name = "Osinski - Von",
                    CreatedAt = new DateTime(2021, 4, 16)
                },
                new Team()
                {
                    Id = 2,
                    Name = "Ernser, Dach and Paucek",
                    CreatedAt = new DateTime(2021, 11, 6)
                },
                new Team()
                {
                    Id= 3,
                    Name = "Heaney - Tillman",
                    CreatedAt = new DateTime(2018, 6, 4)
                }
            };
            var tasks = new List<Task>
            {
                new Task()
                {
                    Id = 0,
                    ProjectId = 0,
                    PerformerId = 0,
                    Name = "Generic Steel Towels",
                    Description = "Magni fugit ut consequatur sit voluptatem eum.",
                    State = TaskState.First,
                    CreatedAt = new DateTime(2018, 9, 1),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 1,
                    ProjectId = 0,
                    PerformerId = 0,
                    Name = "Producer",
                    Description = "Sit ut quo tempora.",
                    State = TaskState.Second,
                    CreatedAt = new DateTime(2018, 4, 19),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 2,
                    ProjectId = 0,
                    PerformerId = 0,
                    Name = "Lake Senior",
                    Description = "Et omnis cumque dolorem qui cupiditate aperiam.",
                    State = TaskState.Third,
                    CreatedAt = new DateTime(2020, 4, 28),
                    FinishedAt = new DateTime(2021, 5, 20)
                },
                new Task()
                {
                    Id = 3,
                    ProjectId = 0,
                    PerformerId = 0,
                    Name = "vertical",
                    Description = "Et iusto aut impedit.",
                    State = TaskState.Fourth,
                    CreatedAt = new DateTime(2020, 4, 2),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 4,
                    ProjectId = 1,
                    PerformerId = 1,
                    Name = "Baby Riel",
                    Description = "Expedita alias unde vitae esse quia voluptatem dolorum ullam aut.",
                    State = TaskState.First,
                    CreatedAt = new DateTime(2019, 11, 1),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 5,
                    ProjectId = 1,
                    PerformerId = 1,
                    Name = "Human Granite",
                    Description = "Ut ut aliquam nihil dignissimos beatae rerum voluptatem et aut.",
                    State = TaskState.Second,
                    CreatedAt = new DateTime(2019, 5, 5),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 6,
                    ProjectId = 1,
                    PerformerId = 1,
                    Name = "mobile",
                    Description = "Placeat esse reprehenderit velit ratione quibusdam officiis dolor expedita.",
                    State = TaskState.Third,
                    CreatedAt = new DateTime(2018, 6, 21),
                    FinishedAt = new DateTime(2021, 6, 21)
                },
                new Task()
                {
                    Id = 7,
                    ProjectId = 1,
                    PerformerId = 1,
                    Name = "Specialist bus",
                    Description = "Laboriosam provident officiis quia quia unde et vitae.",
                    State = TaskState.Fourth,
                    CreatedAt = new DateTime(2019, 1, 31),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 8,
                    ProjectId = 2,
                    PerformerId = 2,
                    Name = "multi-byte",
                    Description = "Recusandae odit et sunt exercitationem maiores sapiente.",
                    State = TaskState.First,
                    CreatedAt = new DateTime(2019, 11, 28),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 9,
                    ProjectId = 2,
                    PerformerId = 2,
                    Name = "Key",
                    Description = "Voluptas rem eius perspiciatis ut sunt animi ratione temporibus magni.",
                    State = TaskState.Second,
                    CreatedAt = new DateTime(2018, 5, 3),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 10,
                    ProjectId = 2,
                    PerformerId = 2,
                    Name = "Berkshire invoice Small Rubber Salad",
                    Description = "Et ut inventore dolorem et ullam vero ratione.",
                    State = TaskState.Third,
                    CreatedAt = new DateTime(2019, 3, 11),
                    FinishedAt = new DateTime(2021, 5, 17)
                },
                new Task()
                {
                    Id = 11,
                    ProjectId = 2,
                    PerformerId = 2,
                    Name = "Saint Martin hack Small",
                    Description = "Est possimus ut doloremque placeat.",
                    State = TaskState.Fourth,
                    CreatedAt = new DateTime(2020, 10, 14),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 12,
                    ProjectId = 3,
                    PerformerId = 3,
                    Name = "Mongolia Ergonomic Rubber Chicken",
                    Description = "Beatae debitis animi quam temporibus accusamus modi.",
                    State = TaskState.First,
                    CreatedAt = new DateTime(2019, 6, 11),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 13,
                    ProjectId = 3,
                    PerformerId = 3,
                    Name = "AI Forward",
                    Description = "Eos nobis eius aut consequuntur ut ea ut eum.",
                    State = TaskState.Second,
                    CreatedAt = new DateTime(2018, 11, 14),
                    FinishedAt = null
                },
                new Task()
                {
                    Id = 14,
                    ProjectId = 3,
                    PerformerId = 3,
                    Name = "Cambridgeshire Avon",
                    Description = "Dignissimos nesciunt non omnis laborum.",
                    State = TaskState.Third,
                    CreatedAt = new DateTime(2018, 6, 28),
                    FinishedAt = new DateTime(2021, 7, 30)
                },
                new Task()
                {
                    Id = 15,
                    ProjectId = 3,
                    PerformerId = 3,
                    Name = "Lari",
                    Description = "Corrupti perferendis vel delectus placeat.",
                    State = TaskState.Fourth,
                    CreatedAt = new DateTime(2020, 4, 13),
                    FinishedAt = null
                }
            };
            modelBuilder.Entity<Project>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<Task>()
                .Property(t => t.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<Team>()
                .Property(t => t.Id)
                .ValueGeneratedNever();
            modelBuilder.Entity<User>()
                .Property(u => u.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Projects)
                .WithOne(p => p.Author);

            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany(u => u.Tasks)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder
                .Entity<Task>()
                .Property(e => e.State)
                .HasConversion<int>();
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
        }
    }
}