﻿using System;
using ProjectStructure.Shared.HttpServices;
using System.Threading.Tasks;

namespace ProjectStructure.ConsoleMenu
{
    public static class Menu
    {
        private static readonly LinqHttpService LinqService = new();
        private static readonly TasksHttpService TasksService = new();
        public static async Task Start()
        {
            Console.Clear();
            Console.WriteLine("---###MENU###---");
            Console.WriteLine("1. Count tasks of current user");
            Console.WriteLine("2. Get all tasks of user with name length < 45");
            Console.WriteLine("3. Get finished this year(2021) tasks of user");
            Console.WriteLine("4. Get users older than 10 sorted by registration date and grouped by team");
            Console.WriteLine("5. Sort by user first name and task name");
            Console.WriteLine("6. Analyze user's last tasks and project");
            Console.WriteLine("7. Analyze project's tasks and team");
            Console.WriteLine("8. Mark random task as completed");
            Console.WriteLine("9. Initialize default data");
            Console.Write("\nWrite chosen menu option: ");

            if (!int.TryParse(Console.ReadLine(), out var menuKey))
            {
                Console.WriteLine("Wrong Input");
                Back();
            }

            Console.Clear();
            switch (menuKey)
            {
                case 1:
                    await CountTasksOfCurrentUser();
                    break;
                case 2:
                    await GetTasksOfCurrentUser();
                    break;
                case 3:
                    await GetFinishedThisYearTasksOfCurrentUser();
                    break;
                case 4:
                    await GetUsersOlderThanTenYearsOld();
                    break;
                case 5:
                    await GetSortedUserByAscendingAndTasksByDescending();
                    break;
                case 6:
                    await AnalyzeUserProjectsAndTasks();
                    break;
                case 7:
                    await AnalyzeProjectTasksAndTeam();
                    break;
                case 8:
                    await MarkRandomTaskWithDelay();
                    break;
                case 9:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Wrong menu option");
                    break;
            }

            Back();
        }


        public static async Task<string> CountTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                var output = "";
                output += "Wrong input";
                return output;
            }

            var result = await LinqService.CountTasksOfCurrentUser(userId);
            var outputString = "";
            foreach (var (key, value) in result)
            {
                outputString += $"ProjectId: {key}, Tasks: {value}";
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
            
        }


        public static async Task<string> GetTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");

            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                var output = "";
                output += "Wrong input";
                return output;
            }

            var result = await LinqService.GetTasksOfCurrentUser(userId);
            var outputString = "";
            foreach (var item in result)
            {
                outputString += $"\n\nName: {item.Name} \nDescription: {item.Description}";
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }


        public static async Task<string> GetFinishedThisYearTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                var output = "";
                output += "Wrong input";
                return output;
            }

            var result = await LinqService.GetFinishedThisYearTasksOfCurrentUser(userId);
            var outputString = "";
            foreach (var (item1, item2) in result)
            {
                outputString += $"\n\nTask ID: {item1}\n Task Name: {item2}";
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> GetUsersOlderThanTenYearsOld()
        {
            var result = await LinqService.GetUsersOlderThanTenYearsOld();
            var outputString = "";
            foreach (var item in result)
            {
                outputString += $"\n\nTeam ID: {item.Id}, Team Name: {item.TeamName}. \nUsers:\n\n";
                foreach (var user in item.UsersOlderThanTenYearsOld)
                {
                    outputString += $"{user.FirstName} {user.LastName} ";
                }
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }


        public static async Task<string> GetSortedUserByAscendingAndTasksByDescending()
        {
            var result = await LinqService.GetSortedUserByAscendingAndTasksByDescending();
            var outputString = "";
            foreach (var item in result)
            {
                outputString += $"\n\nUser First Name: {item.UserName}. Tasks:\n\n";
                foreach (var task in item.SortedTasks)
                {
                    outputString += $"{task.Name}\n\n";
                }
            }
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }


        public static async Task<string> AnalyzeUserProjectsAndTasks()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                var output = "";
                output += "Wrong input";
                return output;
            }

            var result = await LinqService.AnalyzeUserProjectsAndTasks(userId);
            if (result == null)
            {
                var output = "";
                output += "Not Found";
                return output;
            }
            var outputString = "";
            outputString += $"\n\nUser: {result.User.FirstName} {result.User.LastName}\n";
            outputString += $"Last Project: {result.LastProject?.Name}\n";
            outputString += $"Total tasks count: {result.TotalTasksCount}\n";
            outputString += $"Uncompleted and canceled tasks count: {result.TotalUncompletedAndCanceledTasks}\n";
            outputString += $"Longest task: {result.LongestTask?.Name}\n";

            outputString += "\n\nPress any key to go back...";
            return outputString;
        }


        public static async Task<string> AnalyzeProjectTasksAndTeam()
        {
            Console.WriteLine("Write project ID: ");
            if (!int.TryParse(Console.ReadLine(), out var projectId))
            {
                var output = "";
                output += "Wrong input";
                return output;
            }
            var outputString = "";
            var result = await LinqService.AnalyzeProjectTasksAndTeam(projectId);
            outputString += $"\n\nProject: {result.Project?.Name}\n";
            outputString += $"Longest task by description: {result.LongestTaskByDescription?.Description}\n";
            outputString += $"Shortest task by name: {result.ShortestTaskByName?.Name}\n";
            outputString += $"Total team count where project description length > 20 or tasks count < 3: {result.TotalTeamCount}\n";
            outputString += "\n\nPress any key to go back...";
            return outputString;
        }

        public static async Task<string> MarkRandomTaskWithDelay(int delay = 1000)
        {
            var taskId = await TaskMarker.MarkRandomTaskWithDelay(delay);
            return $"\n\n\nRandom task with ID {taskId} marked as finished!";
        }



        public static void Back()
        {
            Console.WriteLine("\nPress any key to go back...");
            Console.ReadKey();
            _ = Start();
        }
    }
}