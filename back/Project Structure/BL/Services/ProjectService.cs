﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.BL.Exceptions;

namespace ProjectStructure.BL.Services
{
    public sealed class ProjectService : BaseService<Project>
    {
        public ProjectService(ProjectStructureDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ProjectDTO> Create (ProjectDTO projectDTO)
        {
            var project = Mapper.Map<Project>(projectDTO);
            Context.Projects.Add(project);
            await Context.SaveChangesAsync();
            return Mapper.Map<ProjectDTO>(project);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var project = await Context.Projects.FindAsync(id);
            if (project == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            Context.Projects.Remove(project);
            await Context.SaveChangesAsync();
        }

        public async Task<ICollection<ProjectDTO>> Get()
        {
            var projects = await Context.Projects.ToListAsync();
            return Mapper.Map<ICollection<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> Get(int id)
        {
            var project = await Context.Projects.FindAsync(id);
            if (project == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }
            return Mapper.Map<ProjectDTO>(project);
        }

        public async System.Threading.Tasks.Task Update(ProjectDTO projectDTO)
        {
            var project = await  Context.Projects.FindAsync(projectDTO.Id);
            if (project == null)
            {
                throw new NotFoundException(nameof(Project), projectDTO.Id);
            }
            project.AuthorId = projectDTO.AuthorId;
            project.CreatedAt = projectDTO.CreatedAt;
            project.Deadline = projectDTO.Deadline;
            project.Name = projectDTO.Name;
            project.TeamId = projectDTO.TeamId;
            Context.Projects.Update(project);
            await Context.SaveChangesAsync();
        }


    }
}