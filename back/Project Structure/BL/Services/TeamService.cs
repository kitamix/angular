﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.DAL.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Exceptions;

namespace ProjectStructure.BL.Services
{
    public sealed class TeamService : BaseService<Team>
    {
        public TeamService(ProjectStructureDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<TeamDTO> Create(TeamDTO teamDTO)
        {
            var team = Mapper.Map<Team>(teamDTO);
            Context.Teams.Add(team);
            await Context.SaveChangesAsync();
            return Mapper.Map<TeamDTO>(team);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var team = await Context.Teams.FindAsync(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            Context.Teams.Remove(team);
            await Context.SaveChangesAsync();
        }

        public async Task<ICollection<TeamDTO>> Get()
        {
            var teams = await Context.Teams.ToListAsync();
            return Mapper.Map<ICollection<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> Get(int id)
        {
            var team = await Context.Teams.FindAsync(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            return Mapper.Map<TeamDTO>(team);
        }

        public async System.Threading.Tasks.Task Update(TeamDTO teamDTO)
        {
            var team = await Context.Teams.FindAsync(teamDTO.Id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), teamDTO.Id);
            }
            team.CreatedAt = teamDTO.CreatedAt;
            team.Name = teamDTO.Name;
            Context.Teams.Update(team);
            await Context.SaveChangesAsync();
        }
    }
}