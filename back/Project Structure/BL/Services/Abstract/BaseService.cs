﻿using AutoMapper;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BL.Services.Abstract
{
    public abstract class BaseService<T> where T : class
    {
        private protected readonly ProjectStructureDbContext Context;
        private protected readonly IMapper Mapper;

        public BaseService(ProjectStructureDbContext context, IMapper mapper)
        {
            Context = context;
            Mapper = mapper;
        }
    }
}