﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectStructure.DAL.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Exceptions;
using Task = ProjectStructure.DAL.Models.Task;

namespace ProjectStructure.BL.Services
{
    public sealed class TaskService : BaseService<Task>
    {
        public TaskService(ProjectStructureDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<TaskDTO> Create(TaskDTO taskDTO)
        {
            var task = Mapper.Map<Task>(taskDTO);
            Context.Tasks.Add(task);
            await Context.SaveChangesAsync();
            return Mapper.Map<TaskDTO>(task);

        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var task = await Context.Tasks.FindAsync(id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Task), id);
            }

            Context.Tasks.Remove(task);
            await Context.SaveChangesAsync();
        }

        public async Task<ICollection<TaskDTO>> Get()
        {
            var tasks = await Context.Tasks.ToListAsync();
            return Mapper.Map<ICollection<TaskDTO>>(tasks);
        }
        public async Task<ICollection<TaskDTO>> GetUncompleted()
        {
            var tasks = await Context.Tasks.Where(t => t.State != TaskState.Third).ToListAsync();
            return Mapper.Map<ICollection<TaskDTO>>(tasks);
        }
        public async Task<TaskDTO> Get(int id)
        {
            var task = await Context.Tasks.FindAsync(id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Task), id);
            }
            return Mapper.Map<TaskDTO>(task);
        }

        public async System.Threading.Tasks.Task Update(TaskDTO taskDTO)
        {
            var task = await Context.Tasks.FindAsync(taskDTO.Id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Task), taskDTO.Id);
            }
            task.CreatedAt = taskDTO.CreatedAt;
            task.Description = taskDTO.Description;
            task.FinishedAt = taskDTO.FinishedAt;
            task.Name = taskDTO.Name;
            task.PerformerId = taskDTO.PerformerId;
            task.ProjectId = taskDTO.ProjectId;
            task.State = Mapper.Map<TaskState>(taskDTO.State);
            Context.Tasks.Update(task);
            await Context.SaveChangesAsync();
        }
    }
}