﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.Shared.Abstract;
using Newtonsoft.Json;
using ProjectStructure.Shared.DTOs;

namespace ProjectStructure.Shared.HttpServices
{
    public class TasksHttpService
    {
        private readonly HttpClient _httpClient = new();

        public TasksHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Tasks/");
        }

        public async Task<List<TaskDTO>> GetAllTasks()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<TaskDTO>>(content);
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<TaskDTO>(content);
        }
        public async Task<List<TaskDTO>> GetAllUncompletedTasks()
        {
            var content = await _httpClient.GetStringAsync("uncompleted");
            return JsonConvert.DeserializeObject<List<TaskDTO>>(content);
        }
        public async Task<TaskDTO> UpdateTask(TaskDTO item)
        {
            var content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json");
            var result = await _httpClient.PutAsync($"{item.Id}", content);
            return JsonConvert.DeserializeObject<TaskDTO>(await result.Content.ReadAsStringAsync());
        }

    }
}