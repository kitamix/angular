﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.Shared.Abstract;

namespace ProjectStructure.Shared.HttpServices
{
    public class LinqHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public LinqHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Linq/");
        }
        public async Task<IDictionary<int, int>> CountTasksOfCurrentUser(int userId)
        {
            var content = await _httpClient.GetStringAsync($"1/{userId}");
            return JsonConvert.DeserializeObject<IDictionary<int, int>>(content);
        }
        public async Task<List<TaskDTO>> GetTasksOfCurrentUser(int userId)
        {
            var content = await _httpClient.GetStringAsync($"2/{userId}");
            return JsonConvert.DeserializeObject<List<TaskDTO>>(content);
        }
        public async Task<List<Tuple<int, string>>> GetFinishedThisYearTasksOfCurrentUser(int userId)
        {
            var content = await _httpClient.GetStringAsync($"3/{userId}");
            return JsonConvert.DeserializeObject<List<Tuple<int, string>>>(content);
        }
        public async Task<IEnumerable<UsersOlderThanTenYearsOldDTO>> GetUsersOlderThanTenYearsOld()
        {
            var content = await _httpClient.GetStringAsync($"4");
            return JsonConvert.DeserializeObject<IEnumerable<UsersOlderThanTenYearsOldDTO>>(content);
        }
        public async Task<IEnumerable<SortedUserByAscendingAndTasksByDescending>> GetSortedUserByAscendingAndTasksByDescending()
        {
            var content = await _httpClient.GetStringAsync($"5");
            return JsonConvert.DeserializeObject<IEnumerable<SortedUserByAscendingAndTasksByDescending>>(content);
        }
        public async Task<UserAdditionalInfoDTO> AnalyzeUserProjectsAndTasks(int userId)
        {
            var content = await _httpClient.GetStringAsync($"6/{userId}");
            return JsonConvert.DeserializeObject<UserAdditionalInfoDTO>(content);
        }
        public async Task<ProjectAdditionalInfoDTO> AnalyzeProjectTasksAndTeam(int projectId)
        {
            var content = await _httpClient.GetStringAsync($"7/{projectId}");
            return JsonConvert.DeserializeObject<ProjectAdditionalInfoDTO>(content);
        }

        public async void LoadTestData()
        {
            var content = new StringContent("");
            await _httpClient.PostAsync("all", content);
        }
    }
}