﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Shared.Abstract;
using ProjectStructure.Shared.DTOs;

namespace ProjectStructure.Shared.HttpServices
{
    public class ProjectsHttpService
    {
        private readonly HttpClient _httpClient = new();

        public ProjectsHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Projects/");
        }

        public async Task<List<ProjectDTO>> GetAllProjects()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<ProjectDTO>>(content);
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<ProjectDTO>(content);
        }
    }
}