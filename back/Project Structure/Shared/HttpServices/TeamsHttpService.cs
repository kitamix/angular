﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ProjectStructure.Shared.Abstract;
using Newtonsoft.Json;
using ProjectStructure.Shared.DTOs;

namespace ProjectStructure.Shared.HttpServices
{
    public class TeamsHttpService
    {
        private readonly HttpClient _httpClient = new();

        public TeamsHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Teams/");
        }

        public async Task<List<TeamDTO>> GetAllTeams()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<TeamDTO>>(content);
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<TeamDTO>(content);
        }
    }
}