﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ProjectStructure.Shared.Abstract;
using Newtonsoft.Json;
using ProjectStructure.Shared.DTOs;

namespace ProjectStructure.Shared.HttpServices
{
    public class UsersHttpService
    {
        private readonly HttpClient _httpClient = new();

        public UsersHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Users/");
        }

        public async Task<List<UserDTO>> GetAllUsers()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<UserDTO>>(content);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<UserDTO>(content);
        }
    }
}