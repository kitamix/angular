﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(
            ProjectService projectService
        )
        {
            _projectService = projectService;
        }

        [HttpPost]
        public async Task<ProjectDTO> Create([FromBody] ProjectDTO projectDTO)
        {
            return await _projectService.Create(projectDTO);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
           await _projectService.Delete(id);
        }
        [HttpGet]
        public async Task<ICollection<ProjectDTO>> Get()
        {
            return await _projectService.Get();
        }
        [HttpGet("{id}")]
        public async Task<ProjectDTO> Get(int id)
        {
            return await _projectService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] ProjectDTO projectDTO)
        {
            await _projectService.Update(projectDTO);
        }
    }
}
