﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("1/{id}")]
        public async Task<IDictionary<int, int>> CountTasksOfCurrentUser(int id)
        {
            return await _linqService.CountTasksOfCurrentUser(id);
        }

        [HttpGet("2/{id}")]
        public async Task<List<TaskDTO>> GetTasksOfCurrentUser(int id)
        {
            return await _linqService.GetTasksOfCurrentUser(id);
        }

        [HttpGet("3/{id}")]
        public async Task<List<Tuple<int, string>>> GetFinishedThisYearTasksOfCurrentUser(int id)
        {
            return await _linqService.GetFinishedThisYearTasksOfCurrentUser(id);
        }

        [HttpGet("4")]
        public async Task<IEnumerable<UsersOlderThanTenYearsOldDTO>> GetTeamsWithUsersOlderThanTen()
        {
            return await _linqService.GetUsersOlderThanTenYearsOld();
        }

        [HttpGet("5")]
        public async Task<IEnumerable<SortedUserByAscendingAndTasksByDescending>>
            GetSortedUserByAscendingAndTasksByDescending()
        {
            return await _linqService.GetSortedUserByAscendingAndTasksByDescending();
        }

        [HttpGet("6/{id}")]
        public async Task<UserAdditionalInfoDTO> AnalyzeUserProjectsAndTasks(int id)
        {
            return await _linqService.AnalyzeUserProjectsAndTasks(id);
        }

        [HttpGet("7/{id}")]
        public async Task<ProjectAdditionalInfoDTO> AnalyzeProjectTasksAndTeam(int id)
        {
            return await _linqService.AnalyzeProjectTasksAndTeam(id);
        }
        [HttpGet("8/{id}")]
        public async Task<List<TaskDTO>> GetUncompletedTasks(int id)
        {
            return await _linqService.GetUncompletedTasks(id);
        }
    }
}