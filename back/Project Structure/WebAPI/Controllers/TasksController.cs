﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(
            TaskService taskService
        )
        {
            _taskService = taskService;
        }

        [HttpPost]
        public async Task<TaskDTO> Create([FromBody] TaskDTO taskDTO)
        {
            return await _taskService.Create(taskDTO);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _taskService.Delete(id);
        }
        [HttpGet]
        public async Task<ICollection<TaskDTO>> Get()
        {
            return await _taskService.Get();
        }
        [HttpGet("uncompleted")]
        public async Task<ICollection<TaskDTO>> GetUncompleted()
        {
            return await _taskService.GetUncompleted();
        }
        [HttpGet("{id}")]
        public async Task<TaskDTO> Get(int id)
        {
            return await _taskService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] TaskDTO taskDTO)
        {
            await _taskService.Update(taskDTO);
        }
    }
}
