import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Team } from '../models/team';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public routePrefix = '/api/teams';

  constructor(private httpService: HttpInternalService) { }

  public getTeams(): Observable<HttpResponse<Team[]>>{
    return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
  }
  public createTeam(team: Team): Observable<HttpResponse<Team>>{
    return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, team);
  }

  public updateTeam(id: number, team: Team): Observable<HttpResponse<Team>>{
      return this.httpService.putFullRequest<Team>(`${this.routePrefix}/${id}`, team);
  }
  public deleteTeam(id: number): Observable<HttpResponse<Team>> {
      return this.httpService.deleteFullRequest<Team>(`${this.routePrefix}/${id}`);
  }
}
