import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpInternalService } from './http-internal.service';
import { Task } from './../models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  public routePrefix = '/api/tasks';

  constructor(private httpService: HttpInternalService) { }

  public getTasks(): Observable<HttpResponse<Task[]>>{
    return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
  }
  public createTask(task: Task): Observable<HttpResponse<Task>>{
    return this.httpService.postFullRequest<Task>(`${this.routePrefix}`, task);
  }

  public updateTask(id: number, task: Task): Observable<HttpResponse<Task>>{
      return this.httpService.putFullRequest<Task>(`${this.routePrefix}/${id}`, task);
  }
  public deleteTask(id: number): Observable<HttpResponse<Task>> {
      return this.httpService.deleteFullRequest<Task>(`${this.routePrefix}/${id}`);
  }
}
