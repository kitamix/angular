import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { User } from '../models/user';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public routePrefix = '/api/users';

  constructor(private httpService: HttpInternalService) { }

  public getUsers(): Observable<HttpResponse<User[]>>{
    return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
  }
  public createUser(user: User): Observable<HttpResponse<User>>{
    return this.httpService.postFullRequest<User>(`${this.routePrefix}`, user);
  }

  public updateUser(id: number, user: User): Observable<HttpResponse<User>>{
      return this.httpService.putFullRequest<User>(`${this.routePrefix}/${id}`, user);
  }
  public deleteUser(id: number): Observable<HttpResponse<User>> {
      return this.httpService.deleteFullRequest<User>(`${this.routePrefix}/${id}`);
  }
}
