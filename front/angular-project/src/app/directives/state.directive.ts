import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appState]'
})
export class StateDirective {
  constructor(private elementRef: ElementRef) {
    elementRef.nativeElement.style.fontWeight = 'bold';
  }
  @HostListener('mouseenter') onMouseEnter() {

    const text = this.elementRef.nativeElement.innerText;
    switch(text) {
      case 'Finished':
        this.elementRef.nativeElement.style.color = '#1CCF1C';
        break;
      case 'Created':
        this.elementRef.nativeElement.style.color = '#0064FE';
        break;
      case 'Cancelled':
        this.elementRef.nativeElement.style.color = '#FF0000';
        break;
      case 'Started':
        this.elementRef.nativeElement.style.color = '#D0A342';
        break;
    }
  }
  @HostListener('mouseleave') onMouseLeave(){
    this.elementRef.nativeElement.style.color = 'black';
  }
}
