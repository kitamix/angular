import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {
  @Input() public team!: Team;
  @Output() public teamToEditEvent = new EventEmitter<number>();
  @Output() public teamToDeleteEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {

  }

  editTeam(){
    console.log("Details edit: " + this.team.id);
    this.teamToEditEvent.emit(this.team.id!);
  }

  deleteTeam(){
    console.log("Details delete: " + this.team.id);
    this.teamToDeleteEvent.emit(this.team.id!);
  }
}
