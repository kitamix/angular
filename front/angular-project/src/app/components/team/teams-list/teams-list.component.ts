import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit {
  @Input() teams: Team[] = [];
  @Output() selectedTeamId = new EventEmitter<number>();
  @Output() selectedTeamToDeleteId = new EventEmitter<number>();
  private unsubscribe$ = new Subject<void>();
  public loadingTeams = false;

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
  }

  public editTeam(teamId: number): void{
    console.log("List edit: " + teamId);
    this.selectedTeamId.emit(teamId);
  }
  public deleteTeam(teamId: number): void{
    console.log("List delete: " + teamId);
    this.selectedTeamToDeleteId.emit(teamId);
  }
}
