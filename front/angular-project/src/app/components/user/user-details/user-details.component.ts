import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  @Input() public user!: User;
  @Output() public userToEditEvent = new EventEmitter<number>();
  @Output() public userToDeleteEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {

  }

  editUser(){
    console.log("Details edit: " + this.user.id);
    this.userToEditEvent.emit(this.user.id!);
  }

  deleteUser(){
    console.log("Details delete: " + this.user.id);
    this.userToDeleteEvent.emit(this.user.id!);
  }
}
