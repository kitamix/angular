import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Subject } from 'rxjs/internal/Subject';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css']
})
export class UsersPageComponent implements OnInit {
  public hideUserCreate: boolean = true;
  public isCreate: boolean = false;
  public users: User[] = [];
  public selectedUser!: User;
  public selectedUserIndex: number = -1;
  public loadingUsers: boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService
  ) { }
  title = "Users";
  ngOnInit(): void {
    this.getUsers();
    console.log(this.users);
  }

  public getUsers(): void {
    this.loadingUsers = true;
    this.userService
        .getUsers()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
            (resp) => {
                this.loadingUsers = false;
                this.users = resp.body!
                console.log(this.users);
            },
            (error) => (this.loadingUsers = false)
        );
  }

  public editUser(userId: number){
    this.loadingUsers = true;
    if(this.hideUserCreate){
      this.hideUserCreate = false;
      this.selectedUserIndex = this.users.findIndex((pr) => pr.id === userId);
      this.selectedUser = this.users[this.users.findIndex((pr) => pr.id === userId)];
    }
    else{
      this.hideUserCreate = true;
      this.hideUserCreate = false;
      this.selectedUserIndex = this.users.findIndex((pr) => pr.id === userId);
      this.selectedUser = this.users[this.users.findIndex((pr) => pr.id === userId)];
    }
    this.loadingUsers = false;
  }

  public deleteUser(userId: number){
    console.log("Page delete: " + userId);
    this.loadingUsers = true;
    this.userService.deleteUser(userId).subscribe(x => {
      console.log("Deleted " + x);
      this.users.splice(this.users.findIndex((pr) => pr.id === userId), 1);
    });
    this.loadingUsers = false;
  }

  showCreate() {
    this.hideUserCreate = false;
    this.selectedUser = {} as User;
    this.selectedUserIndex = -1;
    this.isCreate = true;
  }

  saveUser(newUser: User | null): void{
    this.loadingUsers = true;
    if(newUser){
    if(this.selectedUserIndex === -1){
        newUser.registeredAt = new Date(Date.now());
        this.userService.createUser(newUser).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Created " + x.body?.id);
          newUser.id = x.body?.id;
          this.users.push(newUser);
        });
    } else {
        newUser.id = this.users[this.selectedUserIndex].id;
        
        this.userService.updateUser(this.users[this.selectedUserIndex].id!, newUser).pipe(takeUntil(this.unsubscribe$)).subscribe(x => {
          console.log("Edited " + x.body);
        });
        this.users[this.selectedUserIndex] = newUser;
      }
    }
    this.selectedUserIndex = -1;
    this.hideUserCreate = true;
    this.isCreate = false;
    this.loadingUsers = false;
}

  select(index: number) {
    let user = this.users[index];
    this.selectedUser = {
      teamId: user.teamId,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      registeredAt: user.registeredAt,
      birthDay: user.birthDay
     } as User;
    this.selectedUserIndex = index;
    this.hideUserCreate = false;
    console.log(this.selectedUser)
  }
  canDeactivate() : boolean | Observable<boolean>{
     
    if(!this.hideUserCreate){
        return confirm("You have unsaved changes. Are you sure you want to exit?");
    }
    else{
        return true;
    }
  }
}
