import { Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from 'src/app/models/project';
import { DatePipe } from '@angular/common'
import { ProjectService } from 'src/app/services/project.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css'],
  providers: [DatePipe]
})
export class ProjectFormComponent implements OnInit {
  public projectForm!: FormGroup;
  @Input() project: Project = {} as Project;
  @Input() isCreate!: boolean;
  @Output() projectChange = new EventEmitter<Project | null>();
  public selectedProjectId!: number;
  private unsubscribe$ = new Subject<void>();
  constructor(private datepipe: DatePipe,
              private projectService: ProjectService) { }


  ngOnInit(): void {
    console.log("PROJECT: " + this.project.name)
    let deadline_date = this.datepipe.transform(this.project.deadline, 'yyyy-MM-dd');
    let createdAt_date = this.datepipe.transform(this.project.createdAt, 'yyyy-MM-dd');
    this.projectForm = new FormGroup({
    'name': new FormControl(this.project.name, [
        Validators.required,
    ]),
    'description': new FormControl(this.project.description, [
      Validators.required,
      Validators.maxLength(250)
    ]),
    'deadline': new FormControl(deadline_date,[
        Validators.required,
    ]),
    'teamId': new FormControl(this.project.teamId, [
      Validators.required
    ]),
    'authorId': new FormControl(this.project.authorId,[
        Validators.required
    ]),
    'createdAt': new FormControl(createdAt_date, [
      Validators.required
    ]),
    });
    
  }
  get name() { return this.projectForm.get('name')!; }

  get description() { return this.projectForm.get('description')!; }

  get deadline() { return this.projectForm.get('deadline')!; }

  get teamId() { return this.projectForm.get('teamId')!; }

  get authorId() { return this.projectForm.get('authorId')!; }

  get createdAt() { return this.projectForm.get('createdAt')!; }

  public onSubmit(): void{
    console.log('Emit event');
    let editedProject = this.projectForm.value
    if(this.isCreate){
      editedProject.createdAt = new Date(Date.now()).toJSON();
      console.log("Created: " + editedProject);
    }
    else{
      console.log("Edited " + editedProject);
    }
    this.projectChange.emit(editedProject);
    this.projectForm.reset();
  }

  public goBack(): void{
    console.log('Emit event');
    this.projectChange.emit(null);
    this.project = {} as Project;
    this.projectForm.reset();
  }

}
