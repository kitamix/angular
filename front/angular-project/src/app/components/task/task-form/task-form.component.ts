import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css'],
  providers: [DatePipe]
})
export class TaskFormComponent implements OnInit {
  public taskForm!: FormGroup;
  @Input() task: Task = {} as Task;
  @Input() isCreate!: boolean;
  @Output() taskChange = new EventEmitter<Task | null>();
  public selectedTaskId!: number;
  private unsubscribe$ = new Subject<void>();
  constructor(private datepipe: DatePipe,
              private taskService: TaskService) { }


  ngOnInit(): void {
    console.log("Task: " + this.task.name)
    let createdAt_date = this.datepipe.transform(this.task.createdAt, 'yyyy-MM-dd');
    let finishedAt_date = this.datepipe.transform(this.task.finishedAt, 'yyyy-MM-dd');
    this.taskForm = new FormGroup({
    'projectId': new FormControl(this.task.projectId, [
        Validators.required,
    ]),
    'performerId': new FormControl(this.task.performerId, [
      Validators.required,
    ]),
    'name': new FormControl(this.task.name, [
      Validators.required,
    ]),
    'description': new FormControl(this.task.description, [
      Validators.required,
      Validators.maxLength(100),
    ]),
    'state': new FormControl(this.task.state, [
      Validators.required,
      Validators.max(3),
      Validators.min(0)
    ]),
    'createdAt': new FormControl(createdAt_date, [
      Validators.required,
    ]),
    'finishedAt': new FormControl(finishedAt_date, [
    ])
    });
    
  }
  get projectId() { return this.taskForm.get('projectId')!; }

  get performerId() { return this.taskForm.get('performerId')!; }

  get name() { return this.taskForm.get('name')!; }

  get description() { return this.taskForm.get('description')!; }

  get state() { return this.taskForm.get('state')!; }

  get createdAt() { return this.taskForm.get('createdAt')!; }

  get finishedAt() { return this.taskForm.get('finishedAt')!; }

  public onSubmit(): void{
    console.log('Emit event');
    let editedTask = this.taskForm.value
    if(this.isCreate){
      editedTask.createdAt = new Date(Date.now()).toJSON();
      console.log("Created: " + editedTask);
    }
    else{
      console.log("Edited " + editedTask);
    }
    editedTask.state != 2 ? editedTask.FinishedAt = null : 0;
    this.taskChange.emit(editedTask);
    this.taskForm.reset();
  }

  public goBack(): void{
    console.log('Emit event');
    this.taskChange.emit(null);
    this.task = {} as Task;
    this.taskForm.reset();
  }

}
